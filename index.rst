.. |label| replace:: 1st Vision Lagerampel
.. |snippet| replace:: FvAmpel
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.4.3
.. |maxVersion| replace:: 5.5.7
.. |version| replace:: 2.1.2
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Mit diesem Plugin haben Sie die Möglichkeit die Lagermenge anzuzeigen oder eine Verfügbarkeit/Schwellenwert anzuzeigen der auch auffällig im Kategorielisting und auf der Artikeldetailseite angezeigt wird.

Frontend
--------
Such/KategorieSeite
___________________
Hier wird ein Badges angezeigt, wie Neu oder TIPP.
Die hinterlegte Farbe wird als Farbfeld angezeigt, der Text ist ein Textfeld und ist als default wie folgt gefüllt
:Rot: nicht lagernd
:Geld: wenige lagernd
:Grün: lagernd

Der Text wird ausgeblendet wenn das Feld Lagerbestand anzeigen oder Schwellenwert anzeigen auf Ja steht, dann wird entsprechend der Lagerbestand oder der Schwellenwert angezeigt (z.B. =>10).
Wenn Lagerbestand und Schwellenwert auf JA steht gewinnt der Lagerbestand.
Wenn Icons hinterlegt sind, dann wird das Icon vor dem Text oder Zahl angezeigt.

.. image:: FvAmpel1.png

Beginnend mit der Version 2.1.0 kann die sogenannte On-Demand-Artikel-Anzeige eingeschaltet werden (siehe
Backend-Einstellungen). Wenn eingeschaltet, so wird bei allen Nicht-Abverkaufsartikeln anstelle vom Lagerbestand ein On-Demand Vermerk in grüner Farbe angezeigt (Textbaustein:
 fvOnDemand).

.. image:: FvAmpel1A.png

Artikeldetailsseite(ADS)
________________________
Wie in der Such-/Kategorieansicht beschrieben

.. image:: FvAmpel2.png

Funktion Standard-Versandinfo deaktivieren:
Im Standard von Shopware wird die Versandinfo mit angezeigt. Wenn dies auf JA steht wird dies im Shop ausgeblendet

.. image:: FvAmpel5.png

Suggestsuche
____________
In dem Suchfeld wird ein Punkt in der entsprechenden Farbe angezeigt

.. image:: FvAmpel4.png



Backend
-------

Grundeinstellungen
__________________
:Anzeige Standard-Versandinfo: Hier können Sie den textbaustein der im Standard eingeblendet wird deaktivieren
:Status rot kleiner gleich: Der Standwert ist hier 0 und bei 0 wird der Artikel rot angezeigt
:Status gruen größer gleich: Der Standwert ist hier 10 und von 1 bis 9 wird der Artikel gelb angezeigt und ab 10 Grün

Farben, Icon
____________
:Hexwert "Rot": Hier können Sie den Wert für Rot hinterlegen
:Hexwert "GELB": Hier können Sie den Wert für Gelb hinterlegen
:Hexwert "GRÜN": Hier können Sie den Wert für Grün hinterlegen
:Icon "ROT": Hier können Sie das Icon auswählen für Rot
:Icon "GELB": Hier können Sie das Icon auswählen für Gelb
:Icon "GRÜN": Hier können Sie das Icon auswählen für Grün

Einstellungen ADS (Artikeldetailseite)
______________________________________
:Anzeige Lagerbestand: Bei Ja wird der genaue Lagerbestand (Lagerbestand schlägt Schwellenwert)
:Anzeige Schwellenwert: Bei Ja wird nicht der genaue Lagerbestand angezeigt sondern der Schwellenwert angezeigt
:Anzeige Icon: Bei Ja wird das Icon
:Größe Icon: Hier können Sie die Größe des Icons bestimmen
:verticale Position Icon: Hier können Sie das Icon noch hinrücken

Einstellungen Kat./Suchlisting
____________________________________
:Anzeige Lagerbestand: Bei Ja wird der genaue Lagerbestand (Lagerbestand schlägt Schwellenwert)
:Anzeige Schwellenwert: Bei Ja wird nicht der genaue Lagerbestand angezeigt sondern der Schwellenwert angezeigt
:Anzeige Icon: Bei Ja wird das Icon
:Größe Icon: Hier können Sie die Größe des Icons bestimmen
:verticale Position Icon: Hier können Sie das Icon noch hinrücken

Einstellungen Suggestsuche
__________________________
:Anzeige Punkt: Bei Ja wird in der Suche ein Punkt für die Verfügbarkeit angezeigt
:Größe Punkt: Hier können Sie die größe des Punktes einstellen
:verticale Position Punkt: Hier können Sie den Punkt noch hinrücken

On Demand Option
________________
:On Demand Artikel Anzeige: Bei ja wird die "On Demand"-Option eingeschaltet

.. image:: FvAmpel3.png

Textbausteine
_____________

frontend/detail/image
*********************
:fvBezeichnunglagerbestandgruen: lagernd
:fvBezeichnunglagerbestandrot: nicht lagernd
:fvBezeichnunglagerbestandgelb: wenig lagernd

frontend/listing/box_article
****************************
:fvBezeichnunglagerbestandgruen: lagernd
:fvBezeichnunglagerbestandrot: nicht lagernd
:fvBezeichnunglagerbestandgelb: wenig lagernd

frontend/fvOnDemand
*******************
:fvOnDemand: On Demand

technische Beschreibung
------------------------
keine

Modifizierte Template-Dateien
-----------------------------
:/detail/image.tpl:
:/index/index.tpl:
:/listing/box_article.tpl:
:/listing/product-box/product-badges.tpl:
:/search/ajax.tpl:


